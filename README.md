# Exercice construction de cohorte

Lors d'études observationnels dans de grandes bases de données de santé, un des enjeux principal est la capacité à créer, manipuler et décrire des cohortes de patients rapidement et efficacement. Nous vous proposons à partir des données fictives du PMSI présentes dans le dossier `data` de simuler ce type de création de cohortes.

## Problématique, caractérisation d'une population 🚀

En vous appuyant sur les données du PMSI, créer deux fonctions :

- `cohort_build` de sélectionner une population sur les paramètres suivants : 
    - codes diagnostics principaux, âge, sexe, durée d'hospitalisation 

- `describe_cohort(cohort)` : affiche les caractérstiques principaux des séjours **par individu** de la cohorte en entrée (celle-ci pouvant être un objet ou un dataframe selon le retour choisi pour la fonction 1). 

Rappel : Les clés de jointure pour ces deux tables sont RSA_NUM, ETA_NUM, yeart 

## Disclaimer

Ce problème est très ouvert et potentiellement chronophage : Y consacrer de 2 à 3 heures (4 heures grand maximum).

Nous **n'attendons pas** de description exhaustive, simplement une capacité à illustrer des données de santé riches et complexes par une analyse quantitative simple.

Il sera bienvenu de simplifier le problème en ciblant une catégorie particulière d'actes par exemple.

Nous attendons une présentation de 10-15min d'un rapport au format notebook (`.Rmd` (R) ou `.ipynb` (jupyter notebook)) expliquant votre démarche d'exploration et d'analyse des données.

# Présentation d'une étude portant sur le SNDS

🤓 Lisez une de cette étude effectuée sur le SNDS : https://gitlab.com/has-sante/public/exercices/exercice_epidemio/-/issues/1

👩‍🏫 Préparer une présentation de 30 min sur ce papier (aucun support n'est demandé)

Le type de question que vous pouvez développer est : le type d'étude, les objectifs, la question scientifique, le cadre statistique (s'il y a lieu), les enjeux et les résultats. 

Discuter ces résultats au vu de vos connaissances épidémiologiques et du SNDS. Mettez en lumière les problèmes pratiques que vous voyez à l'implémentation (en code) de cette étude dans les bases de données. Auriez-vous procédé différement ? Comment répliquer avec un faible coup ce genre d'étude pour un sujet similaire ?  
