# %%
import pandas as pd
from pathlib import Path

DATADIR = Path("data")

mco_b = pd.read_csv(DATADIR / "T_MCO_aaB.csv")
mco_c = pd.read_csv(DATADIR / "T_MCO_aaC.csv") 

mco_b.head()

# %%
 
print(mco_b.columns)